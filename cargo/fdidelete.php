<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "fdiinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$fdi_delete = NULL; // Initialize page object first

class cfdi_delete extends cfdi {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5637D871-062C-434D-8CBC-3F90E9A0E316}";

	// Table name
	var $TableName = 'fdi';

	// Page object name
	var $PageObjName = 'fdi_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (fdi)
		if (!isset($GLOBALS["fdi"]) || get_class($GLOBALS["fdi"]) == "cfdi") {
			$GLOBALS["fdi"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["fdi"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'fdi', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $fdi;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($fdi);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("fdilist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in fdi class, fdiinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

// No functions
	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Load List page SQL
		$sSql = $this->SelectSQL();

		// Load recordset
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
		$conn->raiseErrorFn = '';

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->trackno->setDbValue($rs->fields('trackno'));
		$this->desc->setDbValue($rs->fields('desc'));
		$this->sender->setDbValue($rs->fields('sender'));
		$this->receiver->setDbValue($rs->fields('receiver'));
		$this->add->setDbValue($rs->fields('add'));
		$this->shipped->setDbValue($rs->fields('shipped'));
		$this->eta->setDbValue($rs->fields('eta'));
		$this->origin->setDbValue($rs->fields('origin'));
		$this->destination->setDbValue($rs->fields('destination'));
		$this->status->setDbValue($rs->fields('status'));
		$this->weight->setDbValue($rs->fields('weight'));
		$this->img_path->setDbValue($rs->fields('img_path'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->trackno->DbValue = $row['trackno'];
		$this->desc->DbValue = $row['desc'];
		$this->sender->DbValue = $row['sender'];
		$this->receiver->DbValue = $row['receiver'];
		$this->add->DbValue = $row['add'];
		$this->shipped->DbValue = $row['shipped'];
		$this->eta->DbValue = $row['eta'];
		$this->origin->DbValue = $row['origin'];
		$this->destination->DbValue = $row['destination'];
		$this->status->DbValue = $row['status'];
		$this->weight->DbValue = $row['weight'];
		$this->img_path->DbValue = $row['img_path'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// trackno
		// desc
		// sender
		// receiver
		// add
		// shipped
		// eta
		// origin
		// destination
		// status
		// weight
		// img_path

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// trackno
			$this->trackno->ViewValue = $this->trackno->CurrentValue;
			$this->trackno->ViewCustomAttributes = "";

			// sender
			$this->sender->ViewValue = $this->sender->CurrentValue;
			$this->sender->ViewCustomAttributes = "";

			// receiver
			$this->receiver->ViewValue = $this->receiver->CurrentValue;
			$this->receiver->ViewCustomAttributes = "";

			// shipped
			$this->shipped->ViewValue = $this->shipped->CurrentValue;
			$this->shipped->ViewValue = ew_FormatDateTime($this->shipped->ViewValue, 5);
			$this->shipped->ViewCustomAttributes = "";

			// eta
			$this->eta->ViewValue = $this->eta->CurrentValue;
			$this->eta->ViewValue = ew_FormatDateTime($this->eta->ViewValue, 5);
			$this->eta->ViewCustomAttributes = "";

			// origin
			$this->origin->ViewValue = $this->origin->CurrentValue;
			$this->origin->ViewCustomAttributes = "";

			// destination
			$this->destination->ViewValue = $this->destination->CurrentValue;
			$this->destination->ViewCustomAttributes = "";

			// status
			$this->status->ViewValue = $this->status->CurrentValue;
			$this->status->ViewCustomAttributes = "";

			// weight
			$this->weight->ViewValue = $this->weight->CurrentValue;
			$this->weight->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// trackno
			$this->trackno->LinkCustomAttributes = "";
			$this->trackno->HrefValue = "";
			$this->trackno->TooltipValue = "";

			// sender
			$this->sender->LinkCustomAttributes = "";
			$this->sender->HrefValue = "";
			$this->sender->TooltipValue = "";

			// receiver
			$this->receiver->LinkCustomAttributes = "";
			$this->receiver->HrefValue = "";
			$this->receiver->TooltipValue = "";

			// shipped
			$this->shipped->LinkCustomAttributes = "";
			$this->shipped->HrefValue = "";
			$this->shipped->TooltipValue = "";

			// eta
			$this->eta->LinkCustomAttributes = "";
			$this->eta->HrefValue = "";
			$this->eta->TooltipValue = "";

			// origin
			$this->origin->LinkCustomAttributes = "";
			$this->origin->HrefValue = "";
			$this->origin->TooltipValue = "";

			// destination
			$this->destination->LinkCustomAttributes = "";
			$this->destination->HrefValue = "";
			$this->destination->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// weight
			$this->weight->LinkCustomAttributes = "";
			$this->weight->HrefValue = "";
			$this->weight->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $conn, $Language, $Security;
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "fdilist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($fdi_delete)) $fdi_delete = new cfdi_delete();

// Page init
$fdi_delete->Page_Init();

// Page main
$fdi_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$fdi_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var fdi_delete = new ew_Page("fdi_delete");
fdi_delete.PageID = "delete"; // Page ID
var EW_PAGE_ID = fdi_delete.PageID; // For backward compatibility

// Form object
var ffdidelete = new ew_Form("ffdidelete");

// Form_CustomValidate event
ffdidelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ffdidelete.ValidateRequired = true;
<?php } else { ?>
ffdidelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($fdi_delete->Recordset = $fdi_delete->LoadRecordset())
	$fdi_deleteTotalRecs = $fdi_delete->Recordset->RecordCount(); // Get record count
if ($fdi_deleteTotalRecs <= 0) { // No record found, exit
	if ($fdi_delete->Recordset)
		$fdi_delete->Recordset->Close();
	$fdi_delete->Page_Terminate("fdilist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $fdi_delete->ShowPageHeader(); ?>
<?php
$fdi_delete->ShowMessage();
?>
<form name="ffdidelete" id="ffdidelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($fdi_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $fdi_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="fdi">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($fdi_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $fdi->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($fdi->id->Visible) { // id ?>
		<th><span id="elh_fdi_id" class="fdi_id"><?php echo $fdi->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->trackno->Visible) { // trackno ?>
		<th><span id="elh_fdi_trackno" class="fdi_trackno"><?php echo $fdi->trackno->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->sender->Visible) { // sender ?>
		<th><span id="elh_fdi_sender" class="fdi_sender"><?php echo $fdi->sender->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->receiver->Visible) { // receiver ?>
		<th><span id="elh_fdi_receiver" class="fdi_receiver"><?php echo $fdi->receiver->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->shipped->Visible) { // shipped ?>
		<th><span id="elh_fdi_shipped" class="fdi_shipped"><?php echo $fdi->shipped->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->eta->Visible) { // eta ?>
		<th><span id="elh_fdi_eta" class="fdi_eta"><?php echo $fdi->eta->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->origin->Visible) { // origin ?>
		<th><span id="elh_fdi_origin" class="fdi_origin"><?php echo $fdi->origin->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->destination->Visible) { // destination ?>
		<th><span id="elh_fdi_destination" class="fdi_destination"><?php echo $fdi->destination->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->status->Visible) { // status ?>
		<th><span id="elh_fdi_status" class="fdi_status"><?php echo $fdi->status->FldCaption() ?></span></th>
<?php } ?>
<?php if ($fdi->weight->Visible) { // weight ?>
		<th><span id="elh_fdi_weight" class="fdi_weight"><?php echo $fdi->weight->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$fdi_delete->RecCnt = 0;
$i = 0;
while (!$fdi_delete->Recordset->EOF) {
	$fdi_delete->RecCnt++;
	$fdi_delete->RowCnt++;

	// Set row properties
	$fdi->ResetAttrs();
	$fdi->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$fdi_delete->LoadRowValues($fdi_delete->Recordset);

	// Render row
	$fdi_delete->RenderRow();
?>
	<tr<?php echo $fdi->RowAttributes() ?>>
<?php if ($fdi->id->Visible) { // id ?>
		<td<?php echo $fdi->id->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_id" class="fdi_id">
<span<?php echo $fdi->id->ViewAttributes() ?>>
<?php echo $fdi->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->trackno->Visible) { // trackno ?>
		<td<?php echo $fdi->trackno->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_trackno" class="fdi_trackno">
<span<?php echo $fdi->trackno->ViewAttributes() ?>>
<?php echo $fdi->trackno->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->sender->Visible) { // sender ?>
		<td<?php echo $fdi->sender->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_sender" class="fdi_sender">
<span<?php echo $fdi->sender->ViewAttributes() ?>>
<?php echo $fdi->sender->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->receiver->Visible) { // receiver ?>
		<td<?php echo $fdi->receiver->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_receiver" class="fdi_receiver">
<span<?php echo $fdi->receiver->ViewAttributes() ?>>
<?php echo $fdi->receiver->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->shipped->Visible) { // shipped ?>
		<td<?php echo $fdi->shipped->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_shipped" class="fdi_shipped">
<span<?php echo $fdi->shipped->ViewAttributes() ?>>
<?php echo $fdi->shipped->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->eta->Visible) { // eta ?>
		<td<?php echo $fdi->eta->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_eta" class="fdi_eta">
<span<?php echo $fdi->eta->ViewAttributes() ?>>
<?php echo $fdi->eta->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->origin->Visible) { // origin ?>
		<td<?php echo $fdi->origin->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_origin" class="fdi_origin">
<span<?php echo $fdi->origin->ViewAttributes() ?>>
<?php echo $fdi->origin->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->destination->Visible) { // destination ?>
		<td<?php echo $fdi->destination->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_destination" class="fdi_destination">
<span<?php echo $fdi->destination->ViewAttributes() ?>>
<?php echo $fdi->destination->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->status->Visible) { // status ?>
		<td<?php echo $fdi->status->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_status" class="fdi_status">
<span<?php echo $fdi->status->ViewAttributes() ?>>
<?php echo $fdi->status->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($fdi->weight->Visible) { // weight ?>
		<td<?php echo $fdi->weight->CellAttributes() ?>>
<span id="el<?php echo $fdi_delete->RowCnt ?>_fdi_weight" class="fdi_weight">
<span<?php echo $fdi->weight->ViewAttributes() ?>>
<?php echo $fdi->weight->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$fdi_delete->Recordset->MoveNext();
}
$fdi_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div class="btn-group ewButtonGroup">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ffdidelete.Init();
</script>
<?php
$fdi_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$fdi_delete->Page_Terminate();
?>
