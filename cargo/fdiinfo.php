<?php

// Global variable for table object
$fdi = NULL;

//
// Table class for fdi
//
class cfdi extends cTable {
	var $id;
	var $trackno;
	var $desc;
	var $sender;
	var $receiver;
	var $add;
	var $shipped;
	var $eta;
	var $origin;
	var $destination;
	var $status;
	var $weight;
	var $img_path;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'fdi';
		$this->TableName = 'fdi';
		$this->TableType = 'TABLE';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('fdi', 'fdi', 'x_id', 'id', '`id`', '`id`', 3, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// trackno
		$this->trackno = new cField('fdi', 'fdi', 'x_trackno', 'trackno', '`trackno`', '`trackno`', 200, -1, FALSE, '`trackno`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['trackno'] = &$this->trackno;

		// desc
		$this->desc = new cField('fdi', 'fdi', 'x_desc', 'desc', '`desc`', '`desc`', 201, -1, FALSE, '`desc`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['desc'] = &$this->desc;

		// sender
		$this->sender = new cField('fdi', 'fdi', 'x_sender', 'sender', '`sender`', '`sender`', 200, -1, FALSE, '`sender`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['sender'] = &$this->sender;

		// receiver
		$this->receiver = new cField('fdi', 'fdi', 'x_receiver', 'receiver', '`receiver`', '`receiver`', 200, -1, FALSE, '`receiver`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['receiver'] = &$this->receiver;

		// add
		$this->add = new cField('fdi', 'fdi', 'x_add', 'add', '`add`', '`add`', 201, -1, FALSE, '`add`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['add'] = &$this->add;

		// shipped
		$this->shipped = new cField('fdi', 'fdi', 'x_shipped', 'shipped', '`shipped`', 'DATE_FORMAT(`shipped`, \'%Y/%m/%d\')', 133, 5, FALSE, '`shipped`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->shipped->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['shipped'] = &$this->shipped;

		// eta
		$this->eta = new cField('fdi', 'fdi', 'x_eta', 'eta', '`eta`', 'DATE_FORMAT(`eta`, \'%Y/%m/%d\')', 133, 5, FALSE, '`eta`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->eta->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['eta'] = &$this->eta;

		// origin
		$this->origin = new cField('fdi', 'fdi', 'x_origin', 'origin', '`origin`', '`origin`', 200, -1, FALSE, '`origin`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['origin'] = &$this->origin;

		// destination
		$this->destination = new cField('fdi', 'fdi', 'x_destination', 'destination', '`destination`', '`destination`', 200, -1, FALSE, '`destination`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['destination'] = &$this->destination;

		// status
		$this->status = new cField('fdi', 'fdi', 'x_status', 'status', '`status`', '`status`', 200, -1, FALSE, '`status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['status'] = &$this->status;

		// weight
		$this->weight = new cField('fdi', 'fdi', 'x_weight', 'weight', '`weight`', '`weight`', 200, -1, FALSE, '`weight`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['weight'] = &$this->weight;

		// img_path
		$this->img_path = new cField('fdi', 'fdi', 'x_img_path', 'img_path', '`img_path`', '`img_path`', 201, -1, FALSE, '`img_path`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['img_path'] = &$this->img_path;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`fdi`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Check if Anonymous User is allowed
	function AllowAnonymousUser() {
		switch (@$this->PageID) {
			case "add":
			case "register":
			case "addopt":
				return FALSE;
			case "edit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return FALSE;
			case "delete":
				return FALSE;
			case "view":
				return FALSE;
			case "search":
				return FALSE;
			default:
				return FALSE;
		}
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		global $conn;
		$cnt = -1;
		if ($this->TableType == 'TABLE' || $this->TableType == 'VIEW') {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		global $conn;
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Update Table
	var $UpdateTable = "`fdi`";

	// INSERT statement
	function InsertSQL(&$rs) {
		global $conn;
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		global $conn;
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "") {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL) {
		global $conn;
		return $conn->Execute($this->UpdateSQL($rs, $where));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "") {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id') . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType));
		}
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->DeleteSQL($rs, $where));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "fdilist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "fdilist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			return $this->KeyUrl("fdiview.php", $this->UrlParm($parm));
		else
			return $this->KeyUrl("fdiview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			return "fdiadd.php?" . $this->UrlParm($parm);
		else
			return "fdiadd.php";
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		return $this->KeyUrl("fdiedit.php", $this->UrlParm($parm));
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		return $this->KeyUrl("fdiadd.php", $this->UrlParm($parm));
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("fdidelete.php", $this->UrlParm());
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET)) {
			$arKeys[] = @$_GET["id"]; // id

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {
		global $conn;

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->trackno->setDbValue($rs->fields('trackno'));
		$this->desc->setDbValue($rs->fields('desc'));
		$this->sender->setDbValue($rs->fields('sender'));
		$this->receiver->setDbValue($rs->fields('receiver'));
		$this->add->setDbValue($rs->fields('add'));
		$this->shipped->setDbValue($rs->fields('shipped'));
		$this->eta->setDbValue($rs->fields('eta'));
		$this->origin->setDbValue($rs->fields('origin'));
		$this->destination->setDbValue($rs->fields('destination'));
		$this->status->setDbValue($rs->fields('status'));
		$this->weight->setDbValue($rs->fields('weight'));
		$this->img_path->setDbValue($rs->fields('img_path'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// trackno
		// desc
		// sender
		// receiver
		// add
		// shipped
		// eta
		// origin
		// destination
		// status
		// weight
		// img_path
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// trackno
		$this->trackno->ViewValue = $this->trackno->CurrentValue;
		$this->trackno->ViewCustomAttributes = "";

		// desc
		$this->desc->ViewValue = $this->desc->CurrentValue;
		$this->desc->ViewCustomAttributes = "";

		// sender
		$this->sender->ViewValue = $this->sender->CurrentValue;
		$this->sender->ViewCustomAttributes = "";

		// receiver
		$this->receiver->ViewValue = $this->receiver->CurrentValue;
		$this->receiver->ViewCustomAttributes = "";

		// add
		$this->add->ViewValue = $this->add->CurrentValue;
		$this->add->ViewCustomAttributes = "";

		// shipped
		$this->shipped->ViewValue = $this->shipped->CurrentValue;
		$this->shipped->ViewValue = ew_FormatDateTime($this->shipped->ViewValue, 5);
		$this->shipped->ViewCustomAttributes = "";

		// eta
		$this->eta->ViewValue = $this->eta->CurrentValue;
		$this->eta->ViewValue = ew_FormatDateTime($this->eta->ViewValue, 5);
		$this->eta->ViewCustomAttributes = "";

		// origin
		$this->origin->ViewValue = $this->origin->CurrentValue;
		$this->origin->ViewCustomAttributes = "";

		// destination
		$this->destination->ViewValue = $this->destination->CurrentValue;
		$this->destination->ViewCustomAttributes = "";

		// status
		$this->status->ViewValue = $this->status->CurrentValue;
		$this->status->ViewCustomAttributes = "";

		// weight
		$this->weight->ViewValue = $this->weight->CurrentValue;
		$this->weight->ViewCustomAttributes = "";

		// img_path
		$this->img_path->ViewValue = $this->img_path->CurrentValue;
		$this->img_path->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// trackno
		$this->trackno->LinkCustomAttributes = "";
		$this->trackno->HrefValue = "";
		$this->trackno->TooltipValue = "";

		// desc
		$this->desc->LinkCustomAttributes = "";
		$this->desc->HrefValue = "";
		$this->desc->TooltipValue = "";

		// sender
		$this->sender->LinkCustomAttributes = "";
		$this->sender->HrefValue = "";
		$this->sender->TooltipValue = "";

		// receiver
		$this->receiver->LinkCustomAttributes = "";
		$this->receiver->HrefValue = "";
		$this->receiver->TooltipValue = "";

		// add
		$this->add->LinkCustomAttributes = "";
		$this->add->HrefValue = "";
		$this->add->TooltipValue = "";

		// shipped
		$this->shipped->LinkCustomAttributes = "";
		$this->shipped->HrefValue = "";
		$this->shipped->TooltipValue = "";

		// eta
		$this->eta->LinkCustomAttributes = "";
		$this->eta->HrefValue = "";
		$this->eta->TooltipValue = "";

		// origin
		$this->origin->LinkCustomAttributes = "";
		$this->origin->HrefValue = "";
		$this->origin->TooltipValue = "";

		// destination
		$this->destination->LinkCustomAttributes = "";
		$this->destination->HrefValue = "";
		$this->destination->TooltipValue = "";

		// status
		$this->status->LinkCustomAttributes = "";
		$this->status->HrefValue = "";
		$this->status->TooltipValue = "";

		// weight
		$this->weight->LinkCustomAttributes = "";
		$this->weight->HrefValue = "";
		$this->weight->TooltipValue = "";

		// img_path
		$this->img_path->LinkCustomAttributes = "";
		$this->img_path->HrefValue = "";
		$this->img_path->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $conn, $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// trackno
		$this->trackno->EditAttrs["class"] = "form-control";
		$this->trackno->EditCustomAttributes = "";
		$this->trackno->EditValue = ew_HtmlEncode($this->trackno->CurrentValue);
		$this->trackno->PlaceHolder = ew_RemoveHtml($this->trackno->FldCaption());

		// desc
		$this->desc->EditAttrs["class"] = "form-control";
		$this->desc->EditCustomAttributes = "";
		$this->desc->EditValue = ew_HtmlEncode($this->desc->CurrentValue);
		$this->desc->PlaceHolder = ew_RemoveHtml($this->desc->FldCaption());

		// sender
		$this->sender->EditAttrs["class"] = "form-control";
		$this->sender->EditCustomAttributes = "";
		$this->sender->EditValue = ew_HtmlEncode($this->sender->CurrentValue);
		$this->sender->PlaceHolder = ew_RemoveHtml($this->sender->FldCaption());

		// receiver
		$this->receiver->EditAttrs["class"] = "form-control";
		$this->receiver->EditCustomAttributes = "";
		$this->receiver->EditValue = ew_HtmlEncode($this->receiver->CurrentValue);
		$this->receiver->PlaceHolder = ew_RemoveHtml($this->receiver->FldCaption());

		// add
		$this->add->EditAttrs["class"] = "form-control";
		$this->add->EditCustomAttributes = "";
		$this->add->EditValue = ew_HtmlEncode($this->add->CurrentValue);
		$this->add->PlaceHolder = ew_RemoveHtml($this->add->FldCaption());

		// shipped
		$this->shipped->EditAttrs["class"] = "form-control";
		$this->shipped->EditCustomAttributes = "";
		$this->shipped->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->shipped->CurrentValue, 5));
		$this->shipped->PlaceHolder = ew_RemoveHtml($this->shipped->FldCaption());

		// eta
		$this->eta->EditAttrs["class"] = "form-control";
		$this->eta->EditCustomAttributes = "";
		$this->eta->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->eta->CurrentValue, 5));
		$this->eta->PlaceHolder = ew_RemoveHtml($this->eta->FldCaption());

		// origin
		$this->origin->EditAttrs["class"] = "form-control";
		$this->origin->EditCustomAttributes = "";
		$this->origin->EditValue = ew_HtmlEncode($this->origin->CurrentValue);
		$this->origin->PlaceHolder = ew_RemoveHtml($this->origin->FldCaption());

		// destination
		$this->destination->EditAttrs["class"] = "form-control";
		$this->destination->EditCustomAttributes = "";
		$this->destination->EditValue = ew_HtmlEncode($this->destination->CurrentValue);
		$this->destination->PlaceHolder = ew_RemoveHtml($this->destination->FldCaption());

		// status
		$this->status->EditAttrs["class"] = "form-control";
		$this->status->EditCustomAttributes = "";
		$this->status->EditValue = ew_HtmlEncode($this->status->CurrentValue);
		$this->status->PlaceHolder = ew_RemoveHtml($this->status->FldCaption());

		// weight
		$this->weight->EditAttrs["class"] = "form-control";
		$this->weight->EditCustomAttributes = "";
		$this->weight->EditValue = ew_HtmlEncode($this->weight->CurrentValue);
		$this->weight->PlaceHolder = ew_RemoveHtml($this->weight->FldCaption());

		// img_path
		$this->img_path->EditAttrs["class"] = "form-control";
		$this->img_path->EditCustomAttributes = "";
		$this->img_path->EditValue = ew_HtmlEncode($this->img_path->CurrentValue);
		$this->img_path->PlaceHolder = ew_RemoveHtml($this->img_path->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->trackno->Exportable) $Doc->ExportCaption($this->trackno);
					if ($this->desc->Exportable) $Doc->ExportCaption($this->desc);
					if ($this->sender->Exportable) $Doc->ExportCaption($this->sender);
					if ($this->receiver->Exportable) $Doc->ExportCaption($this->receiver);
					if ($this->add->Exportable) $Doc->ExportCaption($this->add);
					if ($this->shipped->Exportable) $Doc->ExportCaption($this->shipped);
					if ($this->eta->Exportable) $Doc->ExportCaption($this->eta);
					if ($this->origin->Exportable) $Doc->ExportCaption($this->origin);
					if ($this->destination->Exportable) $Doc->ExportCaption($this->destination);
					if ($this->status->Exportable) $Doc->ExportCaption($this->status);
					if ($this->weight->Exportable) $Doc->ExportCaption($this->weight);
					if ($this->img_path->Exportable) $Doc->ExportCaption($this->img_path);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->trackno->Exportable) $Doc->ExportCaption($this->trackno);
					if ($this->sender->Exportable) $Doc->ExportCaption($this->sender);
					if ($this->receiver->Exportable) $Doc->ExportCaption($this->receiver);
					if ($this->shipped->Exportable) $Doc->ExportCaption($this->shipped);
					if ($this->eta->Exportable) $Doc->ExportCaption($this->eta);
					if ($this->origin->Exportable) $Doc->ExportCaption($this->origin);
					if ($this->destination->Exportable) $Doc->ExportCaption($this->destination);
					if ($this->status->Exportable) $Doc->ExportCaption($this->status);
					if ($this->weight->Exportable) $Doc->ExportCaption($this->weight);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->trackno->Exportable) $Doc->ExportField($this->trackno);
						if ($this->desc->Exportable) $Doc->ExportField($this->desc);
						if ($this->sender->Exportable) $Doc->ExportField($this->sender);
						if ($this->receiver->Exportable) $Doc->ExportField($this->receiver);
						if ($this->add->Exportable) $Doc->ExportField($this->add);
						if ($this->shipped->Exportable) $Doc->ExportField($this->shipped);
						if ($this->eta->Exportable) $Doc->ExportField($this->eta);
						if ($this->origin->Exportable) $Doc->ExportField($this->origin);
						if ($this->destination->Exportable) $Doc->ExportField($this->destination);
						if ($this->status->Exportable) $Doc->ExportField($this->status);
						if ($this->weight->Exportable) $Doc->ExportField($this->weight);
						if ($this->img_path->Exportable) $Doc->ExportField($this->img_path);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->trackno->Exportable) $Doc->ExportField($this->trackno);
						if ($this->sender->Exportable) $Doc->ExportField($this->sender);
						if ($this->receiver->Exportable) $Doc->ExportField($this->receiver);
						if ($this->shipped->Exportable) $Doc->ExportField($this->shipped);
						if ($this->eta->Exportable) $Doc->ExportField($this->eta);
						if ($this->origin->Exportable) $Doc->ExportField($this->origin);
						if ($this->destination->Exportable) $Doc->ExportField($this->destination);
						if ($this->status->Exportable) $Doc->ExportField($this->status);
						if ($this->weight->Exportable) $Doc->ExportField($this->weight);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		// Enter your code here
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
