<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "fdiinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$fdi_view = NULL; // Initialize page object first

class cfdi_view extends cfdi {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{5637D871-062C-434D-8CBC-3F90E9A0E316}";

	// Table name
	var $TableName = 'fdi';

	// Page object name
	var $PageObjName = 'fdi_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (fdi)
		if (!isset($GLOBALS["fdi"]) || get_class($GLOBALS["fdi"]) == "cfdi") {
			$GLOBALS["fdi"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["fdi"];
		}
		$KeyUrl = "";
		if (@$_GET["id"] <> "") {
			$this->RecKey["id"] = $_GET["id"];
			$KeyUrl .= "&amp;id=" . urlencode($this->RecKey["id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'fdi', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $fdi;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($fdi);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->RecKey["id"] = $this->id->QueryStringValue;
			} elseif (@$_POST["id"] <> "") {
				$this->id->setFormValue($_POST["id"]);
				$this->RecKey["id"] = $this->id->FormValue;
			} else {
				$sReturnUrl = "fdilist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "fdilist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "fdilist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->IsLoggedIn());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->IsLoggedIn());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->IsLoggedIn());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->IsLoggedIn());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->trackno->setDbValue($rs->fields('trackno'));
		$this->desc->setDbValue($rs->fields('desc'));
		$this->sender->setDbValue($rs->fields('sender'));
		$this->receiver->setDbValue($rs->fields('receiver'));
		$this->add->setDbValue($rs->fields('add'));
		$this->shipped->setDbValue($rs->fields('shipped'));
		$this->eta->setDbValue($rs->fields('eta'));
		$this->origin->setDbValue($rs->fields('origin'));
		$this->destination->setDbValue($rs->fields('destination'));
		$this->status->setDbValue($rs->fields('status'));
		$this->weight->setDbValue($rs->fields('weight'));
		$this->img_path->setDbValue($rs->fields('img_path'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->trackno->DbValue = $row['trackno'];
		$this->desc->DbValue = $row['desc'];
		$this->sender->DbValue = $row['sender'];
		$this->receiver->DbValue = $row['receiver'];
		$this->add->DbValue = $row['add'];
		$this->shipped->DbValue = $row['shipped'];
		$this->eta->DbValue = $row['eta'];
		$this->origin->DbValue = $row['origin'];
		$this->destination->DbValue = $row['destination'];
		$this->status->DbValue = $row['status'];
		$this->weight->DbValue = $row['weight'];
		$this->img_path->DbValue = $row['img_path'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// trackno
		// desc
		// sender
		// receiver
		// add
		// shipped
		// eta
		// origin
		// destination
		// status
		// weight
		// img_path

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// trackno
			$this->trackno->ViewValue = $this->trackno->CurrentValue;
			$this->trackno->ViewCustomAttributes = "";

			// desc
			$this->desc->ViewValue = $this->desc->CurrentValue;
			$this->desc->ViewCustomAttributes = "";

			// sender
			$this->sender->ViewValue = $this->sender->CurrentValue;
			$this->sender->ViewCustomAttributes = "";

			// receiver
			$this->receiver->ViewValue = $this->receiver->CurrentValue;
			$this->receiver->ViewCustomAttributes = "";

			// add
			$this->add->ViewValue = $this->add->CurrentValue;
			$this->add->ViewCustomAttributes = "";

			// shipped
			$this->shipped->ViewValue = $this->shipped->CurrentValue;
			$this->shipped->ViewValue = ew_FormatDateTime($this->shipped->ViewValue, 5);
			$this->shipped->ViewCustomAttributes = "";

			// eta
			$this->eta->ViewValue = $this->eta->CurrentValue;
			$this->eta->ViewValue = ew_FormatDateTime($this->eta->ViewValue, 5);
			$this->eta->ViewCustomAttributes = "";

			// origin
			$this->origin->ViewValue = $this->origin->CurrentValue;
			$this->origin->ViewCustomAttributes = "";

			// destination
			$this->destination->ViewValue = $this->destination->CurrentValue;
			$this->destination->ViewCustomAttributes = "";

			// status
			$this->status->ViewValue = $this->status->CurrentValue;
			$this->status->ViewCustomAttributes = "";

			// weight
			$this->weight->ViewValue = $this->weight->CurrentValue;
			$this->weight->ViewCustomAttributes = "";

			// img_path
			$this->img_path->ViewValue = $this->img_path->CurrentValue;
			$this->img_path->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// trackno
			$this->trackno->LinkCustomAttributes = "";
			$this->trackno->HrefValue = "";
			$this->trackno->TooltipValue = "";

			// desc
			$this->desc->LinkCustomAttributes = "";
			$this->desc->HrefValue = "";
			$this->desc->TooltipValue = "";

			// sender
			$this->sender->LinkCustomAttributes = "";
			$this->sender->HrefValue = "";
			$this->sender->TooltipValue = "";

			// receiver
			$this->receiver->LinkCustomAttributes = "";
			$this->receiver->HrefValue = "";
			$this->receiver->TooltipValue = "";

			// add
			$this->add->LinkCustomAttributes = "";
			$this->add->HrefValue = "";
			$this->add->TooltipValue = "";

			// shipped
			$this->shipped->LinkCustomAttributes = "";
			$this->shipped->HrefValue = "";
			$this->shipped->TooltipValue = "";

			// eta
			$this->eta->LinkCustomAttributes = "";
			$this->eta->HrefValue = "";
			$this->eta->TooltipValue = "";

			// origin
			$this->origin->LinkCustomAttributes = "";
			$this->origin->HrefValue = "";
			$this->origin->TooltipValue = "";

			// destination
			$this->destination->LinkCustomAttributes = "";
			$this->destination->HrefValue = "";
			$this->destination->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// weight
			$this->weight->LinkCustomAttributes = "";
			$this->weight->HrefValue = "";
			$this->weight->TooltipValue = "";

			// img_path
			$this->img_path->LinkCustomAttributes = "";
			$this->img_path->HrefValue = "";
			$this->img_path->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "fdilist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($fdi_view)) $fdi_view = new cfdi_view();

// Page init
$fdi_view->Page_Init();

// Page main
$fdi_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$fdi_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var fdi_view = new ew_Page("fdi_view");
fdi_view.PageID = "view"; // Page ID
var EW_PAGE_ID = fdi_view.PageID; // For backward compatibility

// Form object
var ffdiview = new ew_Form("ffdiview");

// Form_CustomValidate event
ffdiview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ffdiview.ValidateRequired = true;
<?php } else { ?>
ffdiview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $fdi_view->ExportOptions->Render("body") ?>
<?php
	foreach ($fdi_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $fdi_view->ShowPageHeader(); ?>
<?php
$fdi_view->ShowMessage();
?>
<form name="ffdiview" id="ffdiview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($fdi_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $fdi_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="fdi">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($fdi->id->Visible) { // id ?>
	<tr id="r_id">
		<td><span id="elh_fdi_id"><?php echo $fdi->id->FldCaption() ?></span></td>
		<td<?php echo $fdi->id->CellAttributes() ?>>
<span id="el_fdi_id">
<span<?php echo $fdi->id->ViewAttributes() ?>>
<?php echo $fdi->id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->trackno->Visible) { // trackno ?>
	<tr id="r_trackno">
		<td><span id="elh_fdi_trackno"><?php echo $fdi->trackno->FldCaption() ?></span></td>
		<td<?php echo $fdi->trackno->CellAttributes() ?>>
<span id="el_fdi_trackno">
<span<?php echo $fdi->trackno->ViewAttributes() ?>>
<?php echo $fdi->trackno->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->desc->Visible) { // desc ?>
	<tr id="r_desc">
		<td><span id="elh_fdi_desc"><?php echo $fdi->desc->FldCaption() ?></span></td>
		<td<?php echo $fdi->desc->CellAttributes() ?>>
<span id="el_fdi_desc">
<span<?php echo $fdi->desc->ViewAttributes() ?>>
<?php echo $fdi->desc->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->sender->Visible) { // sender ?>
	<tr id="r_sender">
		<td><span id="elh_fdi_sender"><?php echo $fdi->sender->FldCaption() ?></span></td>
		<td<?php echo $fdi->sender->CellAttributes() ?>>
<span id="el_fdi_sender">
<span<?php echo $fdi->sender->ViewAttributes() ?>>
<?php echo $fdi->sender->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->receiver->Visible) { // receiver ?>
	<tr id="r_receiver">
		<td><span id="elh_fdi_receiver"><?php echo $fdi->receiver->FldCaption() ?></span></td>
		<td<?php echo $fdi->receiver->CellAttributes() ?>>
<span id="el_fdi_receiver">
<span<?php echo $fdi->receiver->ViewAttributes() ?>>
<?php echo $fdi->receiver->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->add->Visible) { // add ?>
	<tr id="r_add">
		<td><span id="elh_fdi_add"><?php echo $fdi->add->FldCaption() ?></span></td>
		<td<?php echo $fdi->add->CellAttributes() ?>>
<span id="el_fdi_add">
<span<?php echo $fdi->add->ViewAttributes() ?>>
<?php echo $fdi->add->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->shipped->Visible) { // shipped ?>
	<tr id="r_shipped">
		<td><span id="elh_fdi_shipped"><?php echo $fdi->shipped->FldCaption() ?></span></td>
		<td<?php echo $fdi->shipped->CellAttributes() ?>>
<span id="el_fdi_shipped">
<span<?php echo $fdi->shipped->ViewAttributes() ?>>
<?php echo $fdi->shipped->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->eta->Visible) { // eta ?>
	<tr id="r_eta">
		<td><span id="elh_fdi_eta"><?php echo $fdi->eta->FldCaption() ?></span></td>
		<td<?php echo $fdi->eta->CellAttributes() ?>>
<span id="el_fdi_eta">
<span<?php echo $fdi->eta->ViewAttributes() ?>>
<?php echo $fdi->eta->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->origin->Visible) { // origin ?>
	<tr id="r_origin">
		<td><span id="elh_fdi_origin"><?php echo $fdi->origin->FldCaption() ?></span></td>
		<td<?php echo $fdi->origin->CellAttributes() ?>>
<span id="el_fdi_origin">
<span<?php echo $fdi->origin->ViewAttributes() ?>>
<?php echo $fdi->origin->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->destination->Visible) { // destination ?>
	<tr id="r_destination">
		<td><span id="elh_fdi_destination"><?php echo $fdi->destination->FldCaption() ?></span></td>
		<td<?php echo $fdi->destination->CellAttributes() ?>>
<span id="el_fdi_destination">
<span<?php echo $fdi->destination->ViewAttributes() ?>>
<?php echo $fdi->destination->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->status->Visible) { // status ?>
	<tr id="r_status">
		<td><span id="elh_fdi_status"><?php echo $fdi->status->FldCaption() ?></span></td>
		<td<?php echo $fdi->status->CellAttributes() ?>>
<span id="el_fdi_status">
<span<?php echo $fdi->status->ViewAttributes() ?>>
<?php echo $fdi->status->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->weight->Visible) { // weight ?>
	<tr id="r_weight">
		<td><span id="elh_fdi_weight"><?php echo $fdi->weight->FldCaption() ?></span></td>
		<td<?php echo $fdi->weight->CellAttributes() ?>>
<span id="el_fdi_weight">
<span<?php echo $fdi->weight->ViewAttributes() ?>>
<?php echo $fdi->weight->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($fdi->img_path->Visible) { // img_path ?>
	<tr id="r_img_path">
		<td><span id="elh_fdi_img_path"><?php echo $fdi->img_path->FldCaption() ?></span></td>
		<td<?php echo $fdi->img_path->CellAttributes() ?>>
<span id="el_fdi_img_path">
<span<?php echo $fdi->img_path->ViewAttributes() ?>>
<?php echo $fdi->img_path->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
ffdiview.Init();
</script>
<?php
$fdi_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$fdi_view->Page_Terminate();
?>
