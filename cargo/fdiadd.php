<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "fdiinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$fdi_add = NULL; // Initialize page object first

class cfdi_add extends cfdi {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5637D871-062C-434D-8CBC-3F90E9A0E316}";

	// Table name
	var $TableName = 'fdi';

	// Page object name
	var $PageObjName = 'fdi_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (fdi)
		if (!isset($GLOBALS["fdi"]) || get_class($GLOBALS["fdi"]) == "cfdi") {
			$GLOBALS["fdi"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["fdi"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'fdi', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $fdi;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($fdi);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["id"] != "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("fdilist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "fdiview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD;  // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->trackno->CurrentValue = NULL;
		$this->trackno->OldValue = $this->trackno->CurrentValue;
		$this->desc->CurrentValue = NULL;
		$this->desc->OldValue = $this->desc->CurrentValue;
		$this->sender->CurrentValue = NULL;
		$this->sender->OldValue = $this->sender->CurrentValue;
		$this->receiver->CurrentValue = NULL;
		$this->receiver->OldValue = $this->receiver->CurrentValue;
		$this->add->CurrentValue = NULL;
		$this->add->OldValue = $this->add->CurrentValue;
		$this->shipped->CurrentValue = NULL;
		$this->shipped->OldValue = $this->shipped->CurrentValue;
		$this->eta->CurrentValue = NULL;
		$this->eta->OldValue = $this->eta->CurrentValue;
		$this->origin->CurrentValue = NULL;
		$this->origin->OldValue = $this->origin->CurrentValue;
		$this->destination->CurrentValue = NULL;
		$this->destination->OldValue = $this->destination->CurrentValue;
		$this->status->CurrentValue = NULL;
		$this->status->OldValue = $this->status->CurrentValue;
		$this->weight->CurrentValue = NULL;
		$this->weight->OldValue = $this->weight->CurrentValue;
		$this->img_path->CurrentValue = NULL;
		$this->img_path->OldValue = $this->img_path->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->trackno->FldIsDetailKey) {
			$this->trackno->setFormValue($objForm->GetValue("x_trackno"));
		}
		if (!$this->desc->FldIsDetailKey) {
			$this->desc->setFormValue($objForm->GetValue("x_desc"));
		}
		if (!$this->sender->FldIsDetailKey) {
			$this->sender->setFormValue($objForm->GetValue("x_sender"));
		}
		if (!$this->receiver->FldIsDetailKey) {
			$this->receiver->setFormValue($objForm->GetValue("x_receiver"));
		}
		if (!$this->add->FldIsDetailKey) {
			$this->add->setFormValue($objForm->GetValue("x_add"));
		}
		if (!$this->shipped->FldIsDetailKey) {
			$this->shipped->setFormValue($objForm->GetValue("x_shipped"));
			$this->shipped->CurrentValue = ew_UnFormatDateTime($this->shipped->CurrentValue, 5);
		}
		if (!$this->eta->FldIsDetailKey) {
			$this->eta->setFormValue($objForm->GetValue("x_eta"));
			$this->eta->CurrentValue = ew_UnFormatDateTime($this->eta->CurrentValue, 5);
		}
		if (!$this->origin->FldIsDetailKey) {
			$this->origin->setFormValue($objForm->GetValue("x_origin"));
		}
		if (!$this->destination->FldIsDetailKey) {
			$this->destination->setFormValue($objForm->GetValue("x_destination"));
		}
		if (!$this->status->FldIsDetailKey) {
			$this->status->setFormValue($objForm->GetValue("x_status"));
		}
		if (!$this->weight->FldIsDetailKey) {
			$this->weight->setFormValue($objForm->GetValue("x_weight"));
		}
		if (!$this->img_path->FldIsDetailKey) {
			$this->img_path->setFormValue($objForm->GetValue("x_img_path"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->trackno->CurrentValue = $this->trackno->FormValue;
		$this->desc->CurrentValue = $this->desc->FormValue;
		$this->sender->CurrentValue = $this->sender->FormValue;
		$this->receiver->CurrentValue = $this->receiver->FormValue;
		$this->add->CurrentValue = $this->add->FormValue;
		$this->shipped->CurrentValue = $this->shipped->FormValue;
		$this->shipped->CurrentValue = ew_UnFormatDateTime($this->shipped->CurrentValue, 5);
		$this->eta->CurrentValue = $this->eta->FormValue;
		$this->eta->CurrentValue = ew_UnFormatDateTime($this->eta->CurrentValue, 5);
		$this->origin->CurrentValue = $this->origin->FormValue;
		$this->destination->CurrentValue = $this->destination->FormValue;
		$this->status->CurrentValue = $this->status->FormValue;
		$this->weight->CurrentValue = $this->weight->FormValue;
		$this->img_path->CurrentValue = $this->img_path->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->trackno->setDbValue($rs->fields('trackno'));
		$this->desc->setDbValue($rs->fields('desc'));
		$this->sender->setDbValue($rs->fields('sender'));
		$this->receiver->setDbValue($rs->fields('receiver'));
		$this->add->setDbValue($rs->fields('add'));
		$this->shipped->setDbValue($rs->fields('shipped'));
		$this->eta->setDbValue($rs->fields('eta'));
		$this->origin->setDbValue($rs->fields('origin'));
		$this->destination->setDbValue($rs->fields('destination'));
		$this->status->setDbValue($rs->fields('status'));
		$this->weight->setDbValue($rs->fields('weight'));
		$this->img_path->setDbValue($rs->fields('img_path'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->trackno->DbValue = $row['trackno'];
		$this->desc->DbValue = $row['desc'];
		$this->sender->DbValue = $row['sender'];
		$this->receiver->DbValue = $row['receiver'];
		$this->add->DbValue = $row['add'];
		$this->shipped->DbValue = $row['shipped'];
		$this->eta->DbValue = $row['eta'];
		$this->origin->DbValue = $row['origin'];
		$this->destination->DbValue = $row['destination'];
		$this->status->DbValue = $row['status'];
		$this->weight->DbValue = $row['weight'];
		$this->img_path->DbValue = $row['img_path'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// trackno
		// desc
		// sender
		// receiver
		// add
		// shipped
		// eta
		// origin
		// destination
		// status
		// weight
		// img_path

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// trackno
			$this->trackno->ViewValue = $this->trackno->CurrentValue;
			$this->trackno->ViewCustomAttributes = "";

			// desc
			$this->desc->ViewValue = $this->desc->CurrentValue;
			$this->desc->ViewCustomAttributes = "";

			// sender
			$this->sender->ViewValue = $this->sender->CurrentValue;
			$this->sender->ViewCustomAttributes = "";

			// receiver
			$this->receiver->ViewValue = $this->receiver->CurrentValue;
			$this->receiver->ViewCustomAttributes = "";

			// add
			$this->add->ViewValue = $this->add->CurrentValue;
			$this->add->ViewCustomAttributes = "";

			// shipped
			$this->shipped->ViewValue = $this->shipped->CurrentValue;
			$this->shipped->ViewValue = ew_FormatDateTime($this->shipped->ViewValue, 5);
			$this->shipped->ViewCustomAttributes = "";

			// eta
			$this->eta->ViewValue = $this->eta->CurrentValue;
			$this->eta->ViewValue = ew_FormatDateTime($this->eta->ViewValue, 5);
			$this->eta->ViewCustomAttributes = "";

			// origin
			$this->origin->ViewValue = $this->origin->CurrentValue;
			$this->origin->ViewCustomAttributes = "";

			// destination
			$this->destination->ViewValue = $this->destination->CurrentValue;
			$this->destination->ViewCustomAttributes = "";

			// status
			$this->status->ViewValue = $this->status->CurrentValue;
			$this->status->ViewCustomAttributes = "";

			// weight
			$this->weight->ViewValue = $this->weight->CurrentValue;
			$this->weight->ViewCustomAttributes = "";

			// img_path
			$this->img_path->ViewValue = $this->img_path->CurrentValue;
			$this->img_path->ViewCustomAttributes = "";

			// trackno
			$this->trackno->LinkCustomAttributes = "";
			$this->trackno->HrefValue = "";
			$this->trackno->TooltipValue = "";

			// desc
			$this->desc->LinkCustomAttributes = "";
			$this->desc->HrefValue = "";
			$this->desc->TooltipValue = "";

			// sender
			$this->sender->LinkCustomAttributes = "";
			$this->sender->HrefValue = "";
			$this->sender->TooltipValue = "";

			// receiver
			$this->receiver->LinkCustomAttributes = "";
			$this->receiver->HrefValue = "";
			$this->receiver->TooltipValue = "";

			// add
			$this->add->LinkCustomAttributes = "";
			$this->add->HrefValue = "";
			$this->add->TooltipValue = "";

			// shipped
			$this->shipped->LinkCustomAttributes = "";
			$this->shipped->HrefValue = "";
			$this->shipped->TooltipValue = "";

			// eta
			$this->eta->LinkCustomAttributes = "";
			$this->eta->HrefValue = "";
			$this->eta->TooltipValue = "";

			// origin
			$this->origin->LinkCustomAttributes = "";
			$this->origin->HrefValue = "";
			$this->origin->TooltipValue = "";

			// destination
			$this->destination->LinkCustomAttributes = "";
			$this->destination->HrefValue = "";
			$this->destination->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// weight
			$this->weight->LinkCustomAttributes = "";
			$this->weight->HrefValue = "";
			$this->weight->TooltipValue = "";

			// img_path
			$this->img_path->LinkCustomAttributes = "";
			$this->img_path->HrefValue = "";
			$this->img_path->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// trackno
			$this->trackno->EditAttrs["class"] = "form-control";
			$this->trackno->EditCustomAttributes = "";
			$this->trackno->EditValue = ew_HtmlEncode($this->trackno->CurrentValue);
			$this->trackno->PlaceHolder = ew_RemoveHtml($this->trackno->FldCaption());

			// desc
			$this->desc->EditAttrs["class"] = "form-control";
			$this->desc->EditCustomAttributes = "";
			$this->desc->EditValue = ew_HtmlEncode($this->desc->CurrentValue);
			$this->desc->PlaceHolder = ew_RemoveHtml($this->desc->FldCaption());

			// sender
			$this->sender->EditAttrs["class"] = "form-control";
			$this->sender->EditCustomAttributes = "";
			$this->sender->EditValue = ew_HtmlEncode($this->sender->CurrentValue);
			$this->sender->PlaceHolder = ew_RemoveHtml($this->sender->FldCaption());

			// receiver
			$this->receiver->EditAttrs["class"] = "form-control";
			$this->receiver->EditCustomAttributes = "";
			$this->receiver->EditValue = ew_HtmlEncode($this->receiver->CurrentValue);
			$this->receiver->PlaceHolder = ew_RemoveHtml($this->receiver->FldCaption());

			// add
			$this->add->EditAttrs["class"] = "form-control";
			$this->add->EditCustomAttributes = "";
			$this->add->EditValue = ew_HtmlEncode($this->add->CurrentValue);
			$this->add->PlaceHolder = ew_RemoveHtml($this->add->FldCaption());

			// shipped
			$this->shipped->EditAttrs["class"] = "form-control";
			$this->shipped->EditCustomAttributes = "";
			$this->shipped->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->shipped->CurrentValue, 5));
			$this->shipped->PlaceHolder = ew_RemoveHtml($this->shipped->FldCaption());

			// eta
			$this->eta->EditAttrs["class"] = "form-control";
			$this->eta->EditCustomAttributes = "";
			$this->eta->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->eta->CurrentValue, 5));
			$this->eta->PlaceHolder = ew_RemoveHtml($this->eta->FldCaption());

			// origin
			$this->origin->EditAttrs["class"] = "form-control";
			$this->origin->EditCustomAttributes = "";
			$this->origin->EditValue = ew_HtmlEncode($this->origin->CurrentValue);
			$this->origin->PlaceHolder = ew_RemoveHtml($this->origin->FldCaption());

			// destination
			$this->destination->EditAttrs["class"] = "form-control";
			$this->destination->EditCustomAttributes = "";
			$this->destination->EditValue = ew_HtmlEncode($this->destination->CurrentValue);
			$this->destination->PlaceHolder = ew_RemoveHtml($this->destination->FldCaption());

			// status
			$this->status->EditAttrs["class"] = "form-control";
			$this->status->EditCustomAttributes = "";
			$this->status->EditValue = ew_HtmlEncode($this->status->CurrentValue);
			$this->status->PlaceHolder = ew_RemoveHtml($this->status->FldCaption());

			// weight
			$this->weight->EditAttrs["class"] = "form-control";
			$this->weight->EditCustomAttributes = "";
			$this->weight->EditValue = ew_HtmlEncode($this->weight->CurrentValue);
			$this->weight->PlaceHolder = ew_RemoveHtml($this->weight->FldCaption());

			// img_path
			$this->img_path->EditAttrs["class"] = "form-control";
			$this->img_path->EditCustomAttributes = "";
			$this->img_path->EditValue = ew_HtmlEncode($this->img_path->CurrentValue);
			$this->img_path->PlaceHolder = ew_RemoveHtml($this->img_path->FldCaption());

			// Edit refer script
			// trackno

			$this->trackno->HrefValue = "";

			// desc
			$this->desc->HrefValue = "";

			// sender
			$this->sender->HrefValue = "";

			// receiver
			$this->receiver->HrefValue = "";

			// add
			$this->add->HrefValue = "";

			// shipped
			$this->shipped->HrefValue = "";

			// eta
			$this->eta->HrefValue = "";

			// origin
			$this->origin->HrefValue = "";

			// destination
			$this->destination->HrefValue = "";

			// status
			$this->status->HrefValue = "";

			// weight
			$this->weight->HrefValue = "";

			// img_path
			$this->img_path->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->trackno->FldIsDetailKey && !is_null($this->trackno->FormValue) && $this->trackno->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->trackno->FldCaption(), $this->trackno->ReqErrMsg));
		}
		if (!$this->desc->FldIsDetailKey && !is_null($this->desc->FormValue) && $this->desc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->desc->FldCaption(), $this->desc->ReqErrMsg));
		}
		if (!$this->sender->FldIsDetailKey && !is_null($this->sender->FormValue) && $this->sender->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->sender->FldCaption(), $this->sender->ReqErrMsg));
		}
		if (!$this->receiver->FldIsDetailKey && !is_null($this->receiver->FormValue) && $this->receiver->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->receiver->FldCaption(), $this->receiver->ReqErrMsg));
		}
		if (!$this->add->FldIsDetailKey && !is_null($this->add->FormValue) && $this->add->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->add->FldCaption(), $this->add->ReqErrMsg));
		}
		if (!$this->shipped->FldIsDetailKey && !is_null($this->shipped->FormValue) && $this->shipped->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->shipped->FldCaption(), $this->shipped->ReqErrMsg));
		}
		if (!ew_CheckDate($this->shipped->FormValue)) {
			ew_AddMessage($gsFormError, $this->shipped->FldErrMsg());
		}
		if (!$this->eta->FldIsDetailKey && !is_null($this->eta->FormValue) && $this->eta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->eta->FldCaption(), $this->eta->ReqErrMsg));
		}
		if (!ew_CheckDate($this->eta->FormValue)) {
			ew_AddMessage($gsFormError, $this->eta->FldErrMsg());
		}
		if (!$this->origin->FldIsDetailKey && !is_null($this->origin->FormValue) && $this->origin->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->origin->FldCaption(), $this->origin->ReqErrMsg));
		}
		if (!$this->destination->FldIsDetailKey && !is_null($this->destination->FormValue) && $this->destination->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->destination->FldCaption(), $this->destination->ReqErrMsg));
		}
		if (!$this->status->FldIsDetailKey && !is_null($this->status->FormValue) && $this->status->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->status->FldCaption(), $this->status->ReqErrMsg));
		}
		if (!$this->weight->FldIsDetailKey && !is_null($this->weight->FormValue) && $this->weight->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->weight->FldCaption(), $this->weight->ReqErrMsg));
		}
		if (!$this->img_path->FldIsDetailKey && !is_null($this->img_path->FormValue) && $this->img_path->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->img_path->FldCaption(), $this->img_path->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $conn, $Language, $Security;

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// trackno
		$this->trackno->SetDbValueDef($rsnew, $this->trackno->CurrentValue, "", FALSE);

		// desc
		$this->desc->SetDbValueDef($rsnew, $this->desc->CurrentValue, "", FALSE);

		// sender
		$this->sender->SetDbValueDef($rsnew, $this->sender->CurrentValue, "", FALSE);

		// receiver
		$this->receiver->SetDbValueDef($rsnew, $this->receiver->CurrentValue, "", FALSE);

		// add
		$this->add->SetDbValueDef($rsnew, $this->add->CurrentValue, "", FALSE);

		// shipped
		$this->shipped->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->shipped->CurrentValue, 5), ew_CurrentDate(), FALSE);

		// eta
		$this->eta->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->eta->CurrentValue, 5), ew_CurrentDate(), FALSE);

		// origin
		$this->origin->SetDbValueDef($rsnew, $this->origin->CurrentValue, "", FALSE);

		// destination
		$this->destination->SetDbValueDef($rsnew, $this->destination->CurrentValue, "", FALSE);

		// status
		$this->status->SetDbValueDef($rsnew, $this->status->CurrentValue, "", FALSE);

		// weight
		$this->weight->SetDbValueDef($rsnew, $this->weight->CurrentValue, "", FALSE);

		// img_path
		$this->img_path->SetDbValueDef($rsnew, $this->img_path->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Get insert id if necessary
		if ($AddRow) {
			$this->id->setDbValue($conn->Insert_ID());
			$rsnew['id'] = $this->id->DbValue;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "fdilist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($fdi_add)) $fdi_add = new cfdi_add();

// Page init
$fdi_add->Page_Init();

// Page main
$fdi_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$fdi_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var fdi_add = new ew_Page("fdi_add");
fdi_add.PageID = "add"; // Page ID
var EW_PAGE_ID = fdi_add.PageID; // For backward compatibility

// Form object
var ffdiadd = new ew_Form("ffdiadd");

// Validate form
ffdiadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_trackno");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->trackno->FldCaption(), $fdi->trackno->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_desc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->desc->FldCaption(), $fdi->desc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_sender");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->sender->FldCaption(), $fdi->sender->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_receiver");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->receiver->FldCaption(), $fdi->receiver->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_add");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->add->FldCaption(), $fdi->add->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_shipped");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->shipped->FldCaption(), $fdi->shipped->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_shipped");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($fdi->shipped->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_eta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->eta->FldCaption(), $fdi->eta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_eta");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($fdi->eta->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_origin");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->origin->FldCaption(), $fdi->origin->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_destination");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->destination->FldCaption(), $fdi->destination->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_status");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->status->FldCaption(), $fdi->status->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_weight");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->weight->FldCaption(), $fdi->weight->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_img_path");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $fdi->img_path->FldCaption(), $fdi->img_path->ReqErrMsg)) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ffdiadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ffdiadd.ValidateRequired = true;
<?php } else { ?>
ffdiadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $fdi_add->ShowPageHeader(); ?>
<?php
$fdi_add->ShowMessage();
?>
<form name="ffdiadd" id="ffdiadd" class="form-horizontal ewForm ewAddForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($fdi_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $fdi_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="fdi">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($fdi->trackno->Visible) { // trackno ?>
	<div id="r_trackno" class="form-group">
		<label id="elh_fdi_trackno" for="x_trackno" class="col-sm-2 control-label ewLabel"><?php echo $fdi->trackno->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->trackno->CellAttributes() ?>>
<span id="el_fdi_trackno">
<input type="text" data-field="x_trackno" name="x_trackno" id="x_trackno" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($fdi->trackno->PlaceHolder) ?>" value="<?php echo $fdi->trackno->EditValue ?>"<?php echo $fdi->trackno->EditAttributes() ?>>
</span>
<?php echo $fdi->trackno->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->desc->Visible) { // desc ?>
	<div id="r_desc" class="form-group">
		<label id="elh_fdi_desc" for="x_desc" class="col-sm-2 control-label ewLabel"><?php echo $fdi->desc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->desc->CellAttributes() ?>>
<span id="el_fdi_desc">
<textarea data-field="x_desc" name="x_desc" id="x_desc" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($fdi->desc->PlaceHolder) ?>"<?php echo $fdi->desc->EditAttributes() ?>><?php echo $fdi->desc->EditValue ?></textarea>
</span>
<?php echo $fdi->desc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->sender->Visible) { // sender ?>
	<div id="r_sender" class="form-group">
		<label id="elh_fdi_sender" for="x_sender" class="col-sm-2 control-label ewLabel"><?php echo $fdi->sender->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->sender->CellAttributes() ?>>
<span id="el_fdi_sender">
<input type="text" data-field="x_sender" name="x_sender" id="x_sender" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->sender->PlaceHolder) ?>" value="<?php echo $fdi->sender->EditValue ?>"<?php echo $fdi->sender->EditAttributes() ?>>
</span>
<?php echo $fdi->sender->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->receiver->Visible) { // receiver ?>
	<div id="r_receiver" class="form-group">
		<label id="elh_fdi_receiver" for="x_receiver" class="col-sm-2 control-label ewLabel"><?php echo $fdi->receiver->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->receiver->CellAttributes() ?>>
<span id="el_fdi_receiver">
<input type="text" data-field="x_receiver" name="x_receiver" id="x_receiver" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->receiver->PlaceHolder) ?>" value="<?php echo $fdi->receiver->EditValue ?>"<?php echo $fdi->receiver->EditAttributes() ?>>
</span>
<?php echo $fdi->receiver->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->add->Visible) { // add ?>
	<div id="r_add" class="form-group">
		<label id="elh_fdi_add" for="x_add" class="col-sm-2 control-label ewLabel"><?php echo $fdi->add->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->add->CellAttributes() ?>>
<span id="el_fdi_add">
<textarea data-field="x_add" name="x_add" id="x_add" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($fdi->add->PlaceHolder) ?>"<?php echo $fdi->add->EditAttributes() ?>><?php echo $fdi->add->EditValue ?></textarea>
</span>
<?php echo $fdi->add->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->shipped->Visible) { // shipped ?>
	<div id="r_shipped" class="form-group">
		<label id="elh_fdi_shipped" for="x_shipped" class="col-sm-2 control-label ewLabel"><?php echo $fdi->shipped->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->shipped->CellAttributes() ?>>
<span id="el_fdi_shipped">
<input type="text" data-field="x_shipped" name="x_shipped" id="x_shipped" placeholder="<?php echo ew_HtmlEncode($fdi->shipped->PlaceHolder) ?>" value="<?php echo $fdi->shipped->EditValue ?>"<?php echo $fdi->shipped->EditAttributes() ?>>
</span>
<?php echo $fdi->shipped->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->eta->Visible) { // eta ?>
	<div id="r_eta" class="form-group">
		<label id="elh_fdi_eta" for="x_eta" class="col-sm-2 control-label ewLabel"><?php echo $fdi->eta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->eta->CellAttributes() ?>>
<span id="el_fdi_eta">
<input type="text" data-field="x_eta" name="x_eta" id="x_eta" placeholder="<?php echo ew_HtmlEncode($fdi->eta->PlaceHolder) ?>" value="<?php echo $fdi->eta->EditValue ?>"<?php echo $fdi->eta->EditAttributes() ?>>
</span>
<?php echo $fdi->eta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->origin->Visible) { // origin ?>
	<div id="r_origin" class="form-group">
		<label id="elh_fdi_origin" for="x_origin" class="col-sm-2 control-label ewLabel"><?php echo $fdi->origin->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->origin->CellAttributes() ?>>
<span id="el_fdi_origin">
<input type="text" data-field="x_origin" name="x_origin" id="x_origin" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->origin->PlaceHolder) ?>" value="<?php echo $fdi->origin->EditValue ?>"<?php echo $fdi->origin->EditAttributes() ?>>
</span>
<?php echo $fdi->origin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->destination->Visible) { // destination ?>
	<div id="r_destination" class="form-group">
		<label id="elh_fdi_destination" for="x_destination" class="col-sm-2 control-label ewLabel"><?php echo $fdi->destination->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->destination->CellAttributes() ?>>
<span id="el_fdi_destination">
<input type="text" data-field="x_destination" name="x_destination" id="x_destination" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->destination->PlaceHolder) ?>" value="<?php echo $fdi->destination->EditValue ?>"<?php echo $fdi->destination->EditAttributes() ?>>
</span>
<?php echo $fdi->destination->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->status->Visible) { // status ?>
	<div id="r_status" class="form-group">
		<label id="elh_fdi_status" for="x_status" class="col-sm-2 control-label ewLabel"><?php echo $fdi->status->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->status->CellAttributes() ?>>
<span id="el_fdi_status">
<input type="text" data-field="x_status" name="x_status" id="x_status" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->status->PlaceHolder) ?>" value="<?php echo $fdi->status->EditValue ?>"<?php echo $fdi->status->EditAttributes() ?>>
</span>
<?php echo $fdi->status->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->weight->Visible) { // weight ?>
	<div id="r_weight" class="form-group">
		<label id="elh_fdi_weight" for="x_weight" class="col-sm-2 control-label ewLabel"><?php echo $fdi->weight->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->weight->CellAttributes() ?>>
<span id="el_fdi_weight">
<input type="text" data-field="x_weight" name="x_weight" id="x_weight" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($fdi->weight->PlaceHolder) ?>" value="<?php echo $fdi->weight->EditValue ?>"<?php echo $fdi->weight->EditAttributes() ?>>
</span>
<?php echo $fdi->weight->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($fdi->img_path->Visible) { // img_path ?>
	<div id="r_img_path" class="form-group">
		<label id="elh_fdi_img_path" for="x_img_path" class="col-sm-2 control-label ewLabel"><?php echo $fdi->img_path->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $fdi->img_path->CellAttributes() ?>>
<span id="el_fdi_img_path">
<input type="text" data-field="x_img_path" name="x_img_path" id="x_img_path" placeholder="<?php echo ew_HtmlEncode($fdi->img_path->PlaceHolder) ?>" value="<?php echo $fdi->img_path->EditValue ?>"<?php echo $fdi->img_path->EditAttributes() ?>>
</span>
<?php echo $fdi->img_path->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ffdiadd.Init();
</script>
<?php
$fdi_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$fdi_add->Page_Terminate();
?>
