<?php
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
	// $db = new mysqli('207.174.213.126','colossal_baby','beauty4ashes', 'colossal_baby');
	$db = new mysqli('51.141.99.83','root','b13ss3d1.', 'cargo');
	// $db = new mysqli('localhost','root','', 'fdi');

	if($db->connect_errno > 0){
	    die('Unable to connect to database [' . $db->connect_error . ']');
	}

	if(isset($_GET['trackno'])){
	  $trkno = mysqli_real_escape_string($db, $_GET['trackno']);
	  $query = "SELECT * FROM fdi WHERE trackno = '$trkno'";
	  if(!$result = $db->query($query)){
	      die('There was an error running the query [' . $db->error . ']');
	  }
	  if(mysqli_num_rows($result) > 0){
	    $r = mysqli_fetch_array($result);
	    list($id, $trackno,$desc,$sender,$receiver,$add,$shipped,$eta,$origin,$destination,$status,$weight,$photo) = $r;
	    if (isset($photo) && strlen($photo) > 6) {
	    	$photo = "<figure class='' style='border-radius: 10px; width: 200px;'><img style='width: 100%;' src='$photo' /></figure>";
	    } else {
	    	$photo = 'Not Available';
	    }
	    $o = ""
	      . "<div>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Tracking Number: </strong> $trackno</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Description: </strong> $desc</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Sender</strong> $sender</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Receiver</strong> $receiver</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Receiver Address :</strong> $add</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Shipped On :</strong> $shipped</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Estimated Delivery Time :</strong> $eta</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Origin :</strong> $origin</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Destination :</strong>$destination </p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Status :</strong> $status</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Weight :</strong> $weight</p>"
	      .  "<p style='width: 630px;'><strong style='color: green; float: left; min-width:170px'>Package Photo:</strong> $photo</p>"

	      .  "</div>";
	      //MARKER;
	  }else{
	    $o = "<p style='width: 630px;'>There are no records for $trkno . Please try a valid tracking number</p>";
	  }

	  $query2 = "SELECT * FROM statuses WHERE trackno = '$trkno' ORDER BY timestamp DESC";
	  if(!$statuses = $db->query($query2)){
	      die('There was an error running the query [' . $db->error . ']');
	  }
	  if(mysqli_num_rows($statuses) > 0){
	  	$sString = '';
	  	while ($sta = $statuses->fetch_array()) {
	  		# code...
	  		list($id, $tracking_number, $status, $location, $timestamp) = $sta;
	  		$sString 	.= "<tr>"
									. "<td>$status</td>"
									. "<td class='align-center'>$location</td>"
									. "<td class='align-center'> $timestamp</td>"
								  . "</tr>";
	  	}
	  }


	}else{
	  // $o =null;
    $o = "<p style='width: 630px;'>There are no records for $trkno . Please try a valid tracking number</p>";
	}



?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<?php include 'inc/head.php'; ?>

</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">



		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

				<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- MAIN TITLE -->
		<div class="main-title">
			<div class="container">
				<h1 class="main-title__primary">Tracker</h1>
				<h3 class="main-title__secondary">Know exactly where your cargo/freight is at all times</h3>
			</div>
		</div><!-- /.main-title -->

		<!-- BREADCRUMBS -->
		<div class="breadcrumbs">
			<div class="container">
				<span>
					<a class="home" href="/" title="Go to ASAP Express." rel="v:url">New World Express</a>
				</span>
				<span>
					<span>Tracking</span>
				</span>
			</div>
		</div><!-- /.breadcrumbs -->

		<div class="container">

			<div class="row">

				<div class="col-sm-8 col-sm-offset-2 margin-bottom-60">

					<form method="get" action="/tracking_result.php" class="trackForm">
						<input type="text" name="trackno" placeholder="Enter your tracking code here..." id="name" value="">
						<button type="submit" class="btn btn-info">Track</button>
					</form>

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<div class="container">

			<div class="row margin-bottom-30">

				<div class="col-sm-9">

					<h3 class="widget-title margin-top-0">
						Track your cargo: <?php echo $_GET['trackno']; ?>
					</h3>
					<br >
					<?php echo $o; ?>

					<h4>Expected arrival: <?php echo @$eta; ?></h4>

					<p>
						The table below displays all the way points and distribution centers your cargo passes through, including IN and OUT times. If you have any questions regarding your cargo, please don't hesitate to <a href="contact.php">contact us</a>.
					</p>


					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Status</th>
									<th class="align-center">In</th>
									<th class="align-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php echo @$sString; ?>
							</tbody>
						</table>
					</div><!-- /.table-responsive -->

				</div><!-- /.col -->

				<div class="col-sm-3 hentry">

					<?php require './inc/address-bar.php'; ?>
				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/SimpleMap.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>
