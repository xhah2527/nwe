<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'inc/head.php'; ?>

</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">


		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

				<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- MAIN TITLE -->
		<div class="main-title">
			<div class="container">
				<h1 class="main-title__primary">Services</h1>
				<h3 class="main-title__secondary">What we do and what can you expect from us</h3>
			</div>
		</div><!-- /.main-title -->

		<!-- BREADCRUMBS -->
		<div class="breadcrumbs">
			<div class="container">
				<span>
					<a class="home" href="index.html" title="Go to ASAP Express." rel="v:url">ASAP Express</a>
				</span>
				<span>
					<span>Services</span>
				</span>
			</div>
		</div><!-- /.breadcrumbs -->

		<div class="container">

			<div class="row">

				<main class="col-xs-12 col-md-9 col-md-push-3">

					<div class="row margin-bottom-30">

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/17-360x240.jpg 360w, images/demo/17.jpg 848w" src="images/demo/17-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="ground_transport.html">Ground Transport</a>
									</h5>
									<p>
										Ground transport and freight forwarding services, along with other ancillary services, helps us to provide tailor made logistic solutions to our clients, which we design as per their requirements and business ecosystem to help cut their cost, …
									</p>
									<p>
										<a class="read-more" href="ground_transport.html">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/30-360x240.jpg 360w, images/demo/30.jpg 848w" src="images/demo/30-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="#">Logistic Service</a>
									</h5>
									<p>
										We provide logistic services in the nation, whether it is freight transportation, supply chain solutions, warehousing and distribution, customer resource area services, customs, security and insurance, temperature controlled logistics, industry …
									</p>
									<p>
										<a class="read-more" href="#">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="trucking_service.#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/42-360x240.jpg 360w, images/demo/42.jpg 848w" src="images/demo/42-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="#">Trucking Service</a>
									</h5>
									<p>
										Our trucking service is based on the principle of providing our clients with the flexibility and reliability, to move legal load across town, or across the country, whenever they want. Our in-house team of experienced …
									</p>
									<p>
										<a class="read-more" href="#">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

					</div><!-- /.row -->

					<div class="row margin-bottom-30">

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/31-360x240.jpg 360w, images/demo/31.jpg 848w" src="images/demo/31-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="#">Warehousing</a>
									</h5>
									<p>
										Our warehousing and distribution services are regularly audited and analyzed to ensure that it meets the contemporary business models, and any and all upgrades are made to ensure our services continue to help our clients meet their logistic …
									</p>
									<p>
										<a class="read-more" href="#">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/32-360x240.jpg 360w, images/demo/32.jpg 848w" src="images/demo/32-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="#">Cargo</a>
									</h5>
									<p>
										Team of cargo experts are always available to help you with any queries you might have, or if you want to consult in length your logistic requirements. We would study your requirements and provide you with a quote that would not only suit your …
									</p>
									<p>
										<a class="read-more" href="#">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

						<div class="col-sm-4">

							<div class="page-box page-box--block">
								<a href="#" class="page-box__picture">
									<img alt="Ground Transport" class="wp-post-image" sizes="(min-width: 781px) 360px, calc(100vw - 30px)" srcset="images/demo/4-360x240.jpg 360w, images/demo/4.jpg 848w" src="images/demo/4-360x240.jpg">
								</a>
								<div class="page-box__content">
									<h5 class="page-box__title text-uppercase">
										<a href="#">Storage</a>
									</h5>
									<p>
										We take pride in catering to a broad range of clientele throughout the country with our warehousing services, which is comprehensive, reliable and flexible – qualities that are essential to help businesses in this market. Our experienced experts …
									</p>
									<p>
										<a class="read-more" href="#">Read more</a>
									</p>
								</div>
							</div>

						</div><!-- /.col -->

					</div><!-- /.row -->

				</main>

				<div class="col-xs-12 col-md-3 col-md-pull-9">

					<div class="sidebar widget_nav_menu">
						<ul class="menu" id="menu-services-menu">
							<li class="current-menu-item">
								<a href="services.php">All Services</a>
							</li>
							<li>
								<a href="ground_transport.html">Ground Transport</a>
							</li>
							<li>
								<a href="">Cargo</a>
							</li>
							<li>
								<a href="">Warehousing</a>
							</li>
							<li>
								<a href="">Logistic Service</a>
							</li>
							<li>
								<a href="">Trucking Service</a>
							</li>
							<li>
								<a href="">Storage</a>
							</li>
						</ul>
					</div><!-- /.sidebar -->

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>