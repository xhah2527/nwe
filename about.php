<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/head.php'; ?>

</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">

		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

				<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- MAIN TITLE -->
		<div class="main-title">
			<div class="container">
				<h1 class="main-title__primary">About Us</h1>
				<h3 class="main-title__secondary">Something about us from a little bit of different perspective</h3>
			</div>
		</div><!-- /.main-title -->

		<!-- BREADCRUMBS -->
		<div class="breadcrumbs">
			<div class="container">
				<span>
					<a class="home" href="/" title="Go to New World Express." rel="v:url">New World Express</a>
				</span>
				<span>
					<span>About Us</span>
				</span>
			</div>
		</div><!-- /.breadcrumbs -->

		<div class="container">

			<div class="row margin-bottom-30">

				<div class="col-sm-6">

					<p>
						We take pride in being regarded as one of the most reliable and affordable logistic and warehousing service providers in the country. As a third party logistic service provider, we excel at a range of logistic services, which includes trucking services, warehousing services, logistic services, and a range of other ancillary services. We have years of experience in the business of logistics, warehousing, distribution, trucking and supply chain management services, and aim to provide our clients with convenience, reliability and affordability through our premium logistic services.
					</p>

					<p>
						Our team of experts at all levels of our services have years of experience backing them, which adds the credibility of an expert workforce. This also helps us in cutting down response time, and providing punctual delivery and services at all times, whether it is trucking service or warehousing services. Our goal is to make a positive difference in your business through our services, and build long term relationship with you. Our commitment to our clients can be seen by the amount of emphasis we lay on team work, customer support services and making technological upgrades in our logistic process and equipment from time to time.
					</p>

					<p>
						Our experience in all the fields we serve in, and the range of services we provide, makes us one of the most comprehensive logistic service providers in the nation. And, with the help of continuous support and trust of our clients, we aim to stay at the top of the game, and humbly so. Our sophisticated systems, neatly designed logistic process, state of the art logistic tools and equipment, most advanced carriers, custom tailored services, and dedication to keep the costs low for end users, help us to provide logistic solution that aligns well with our clients’ requirements. We welcome you to our site, and request you to consult with our logistic experts for your logistic needs, and rest assured of getting done.
					</p>

					<p>
						We have years of experience in the business of logistics, warehousing, distribution, trucking and supply chain management services, and aim to provide our clients with convenience, reliability and affordability through our premium logistic services.
					</p>

				</div><!-- /.col -->

				<div class="col-sm-6">

					<p>
						<a href="images/demo/5.jpg">
							<img alt="placeholder" src="images/demo/5.jpg" class="alignnone">
						</a>
					</p>
					<p>
						<a href="images/demo/34.jpg">
							<img alt="placeholder" src="images/demo/34.jpg" class="alignnone">
						</a>
					</p>

				</div><!-- /.col -->

			</div><!-- /.row -->
		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>