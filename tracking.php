<!DOCTYPE html>
<html lang="en">
<head>
 	<?php include 'inc/head.php'; ?>

</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">



		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

				<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- MAIN TITLE -->
		<div class="main-title">
			<div class="container">
				<h1 class="main-title__primary">Tracker</h1>
				<h3 class="main-title__secondary">Know exactly where your cargo/freight is at all times</h3>
			</div>
		</div><!-- /.main-title -->

		<!-- BREADCRUMBS -->
		<div class="breadcrumbs">
			<div class="container">
				<span>
					<a class="home" href="/" title="Go to New World Express." rel="v:url">New World Express</a>
				</span>
				<span>
					<span>Tracking</span>
				</span>
			</div>
		</div><!-- /.breadcrumbs -->

		<div class="container">

			<div class="row">

				<div class="col-sm-8 col-sm-offset-2 margin-bottom-60">

					<p>
						Enter your tracking code below and click the "Track" button to find out exactly where your cargo is right now and when it will arrive at its final destination.
					</p>

					<form method="get" action="/tracking_result.php" class="trackForm clearfix">
						<input type="text" name="trackno" placeholder="Enter your tracking code here..." id="name">
						<button type="submit" class="btn btn-info">Track</button>
					</form>

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/SimpleMap.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>