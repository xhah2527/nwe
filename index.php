<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'inc/head.php'; ?>


</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">

		<!-- TOP BAR -->
		<!-- /.top -->

		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

			<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- JUMBOTRON -->
		<div class="jumbotron jumbotron--with-captions">

			<div data-interval="5000" data-ride="carousel" id="headerCarousel" class="carousel slide">

				<div class="carousel-inner">

					<div class="item">
						<img alt="UNBEATABLE TRUCKING AND TRANSPORT SERVICES" sizes="100vw" src="images/demo/slider_1_1920.jpg">
						<div class="container">
							<div class="jumbotron-content">
								<div class="jumbotron-content__title">
								</div>
								<div class="jumbotron-content__description">
									<p class="p1">
										<span class="s1">Starting from loading to unloading and maintaining the highest standards in terms of safety while in transit, we take nothing to chance.</span>
									</p>
									<p>
										<a target="_self" href="services.php" class="btn btn-primary">OUR SERVICES</a> &nbsp;<a target="_self" href="contact.php" class="btn btn-secondary">GET IN TOUCH</a>
									</p>
									<div class="w69b-screencastify-mouse"></div>
								</div>
							</div>
						</div>
					</div><!-- /.item -->

					<div class="item">
						<img alt="CRANE TO TRAIN, WE DO EVERYTHING" sizes="100vw" src="images/demo/slider_2_1920.jpg">
						<div class="container">
							<div class="jumbotron-content">
								<div class="jumbotron-content__title">
								</div>
								<div class="jumbotron-content__description">
									<p class="p1">
										<span class="s1">Starting from loading to unloading and maintaining the highest standards in terms of safety while in transit, we take nothing to chance.</span>
									</p>
									<a target="_self" href="#" class="btn btn-primary">READ MORE</a>
									<div class="w69b-screencastify-mouse"></div>
								</div>
							</div>
						</div>
					</div><!-- /.item -->

					<div class="item active">
						<img alt="GROUND OR AIR, THERE IS NO STOPPING FOR US" sizes="100vw" src="images/demo/slider_3_1920.jpg">
						<div class="container">
							<div class="jumbotron-content">
								<div class="jumbotron-content__title">
								</div>
								<div class="jumbotron-content__description">
									<p class="p1">
										<span class="s1">Starting from loading to unloading and maintaining the highest standards in terms of safety while in transit, we take nothing to chance.</span>
									</p>
									<a target="_self" href="#" class="btn btn-primary">MORE DETAILS</a>
									<div class="w69b-screencastify-mouse"></div>
								</div>
							</div>
						</div>
					</div><!-- /.item -->

				</div><!-- /.carousel-inner -->

				<div class="container">

					<a data-slide="prev" role="button" href="#headerCarousel" class="left jumbotron__control">
						<i class="fa  fa-caret-left"></i>
					</a>
					<a data-slide="next" role="button" href="#headerCarousel" class="right jumbotron__control">
						<i class="fa  fa-caret-right"></i>
					</a>
				</div><!-- /.container -->

			</div><!-- /.carousel -->

		</div><!-- /.jumbotron -->

		<!-- OUR SERVICES -->
		<div class="container" role="main">


			<div class="row">

				<div class="col-sm-12">

					<h3 class="widget-title big lined">
						<span>OUR SERVICES</span>
					</h3>

				</div><!-- /.col -->

			</div><!-- /.row -->

			<div class="row">

				<div class="col-sm-4">

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-dropbox"></i>
							<h4 class="icon-box__title">PACKAGING AND STORAGE</h4>
							<span class="icon-box__subtitle">We can package and store your things.</span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-archive"></i>
							<h4 class="icon-box__title">WAREHOUSING</h4>
							<span class="icon-box__subtitle">We have top notch security and loads of space. Store your stuff at our warehouse.</span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

				</div><!-- /.col -->

				<div class="col-sm-4">

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-truck"></i>
							<h4 class="icon-box__title">CARGO</h4>
							<span class="icon-box__subtitle">Let us transport your things from point A to point B fast and securely. </span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

					<div class="widget widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-home"></i>
							<h4 class="icon-box__title">DOOR-TO-DOOR DELIVERY</h4>
							<span class="icon-box__subtitle">Do you need something delivered? We are what you are looking for! </span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

				</div><!-- /.col -->

				<div class="col-sm-4">

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-globe"></i>
							<h4 class="icon-box__title">WORLDWIDE TRANSPORT</h4>
							<span class="icon-box__subtitle">We can transport your things anywhere in the world. </span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-road"></i>
							<h4 class="icon-box__title">GROUND TRANSPORT</h4>
							<span class="icon-box__subtitle">Transport your things with our super moving vans.</span>
						</a>
					</div><!-- /.widget_pw_icon_box -->

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.conainer -->

		<!-- CTA -->
		<div class="cta">

			<div class="container">

				<div class="row">

					<div class="col-md-12">

						<div class="call-to-action">
							<div class="call-to-action__text">
								Not sure which solution fits you business needs?
							</div>
							<div class="call-to-action__button">
								<a target="_blank" href="contact.php" class="btn btn-primary">CONTACT OUR SALES TEAM</a>
							</div>
						</div><!-- /.call-to-action -->

					</div><!-- /.col -->

				</div><!-- /.row -->

			</div><!-- /.container -->

		</div><!-- /.cta -->

		<!-- OUR PARTNERS -->
		<div class="container">

			<div class="row margin-bottom-60">

				<div class="col-sm-12">

					<div class="widget_text">

						<h3 class="widget-title lined big">
							<span>OUR PARTNERS</span>
						</h3>
						<div class="logo-panel">
							<div class="row">
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_1.png"></a>
								</div>
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_2.png"></a>
								</div>
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_3.png"></a>
								</div>
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_4.png"></a>
								</div>
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_5.png"></a>
								</div>
								<div class="col-xs-12  col-sm-2">
									<a href="#"><img alt="Client" src="images/demo/logo_6.png"></a>
								</div>
							</div><!-- /.row -->
						</div><!-- /.logo-panel -->

					</div><!-- /.widget_text -->

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/SimpleMap.js"></script>
	<script src="js/NumberCounter.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>
