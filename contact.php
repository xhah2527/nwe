<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/head.php'; ?>

</head>
<body>

	<!-- MAIN PAGE CONTAINER -->
	<div class="boxed-container">



		<!-- HEADER -->
		<div class="header__container">

			<div class="container">

				<?php include 'inc/menu.php'; ?>

			</div><!-- /.container -->

		</div><!-- /.header__container -->

		<!-- MAIN TITLE -->
		<div class="main-title">
			<div class="container">
				<h1 class="main-title__primary">Contact Us</h1>
				<h3 class="main-title__secondary">We are waiting you to get in touch with us</h3>
			</div>
		</div><!-- /.main-title -->

		<!-- BREADCRUMBS -->
		<div class="breadcrumbs margin-bottom-0">
			<div class="container">
				<span>
					<a class="home" href="/" title="Go to New World Express." rel="v:url">New World Express</a>
				</span>
				<span>
					<span>Contact Us</span>
				</span>
			</div>
		</div><!-- /.breadcrumbs -->

		<div class="container">

			<div class="row margin-bottom-30">

				<div class="col-sm-3 hentry">

					<?php require './inc/address-bar.php'; ?>
				</div><!-- /.col -->

				<div class="col-sm-9">

					<h3 class="widget-title margin-top-0">
						SEND US AN EMAIL, OR THREE
					</h3>

					<form data-toggle="validator" method="post" action="form.php" class="aSubmit">
						<div style="display:none"><input type="text" name="maximus" value=""></div>
						<input type="hidden" name="theSubject" value="CargoPress: Contact Request">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<div class="form-group">
									<input type="text" placeholder="First Name*" aria-invalid="false" aria-required="true" size="40" value="" name="your-name" required>
								</div>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="form-group">
									<input type="text" placeholder="Last Name*" aria-invalid="false" aria-required="true" size="40" value="" name="last-name" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<div class="form-group">
									<input type="email" placeholder="E-mail address*" aria-invalid="false" aria-required="true" size="40" value="" name="your-email" required>
								</div>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="form-group">
									<input type="tel" placeholder="Phone Number" aria-invalid="false" aria-required="true" size="40" value="" name="your-phone">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<input type="text" placeholder="Subject*" aria-invalid="false" size="40" value="" name="your-subject" required>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<textarea placeholder="Message*" aria-invalid="false" rows="10" cols="40" name="your-message" required></textarea>
								</div>
								<input type="submit" class="btn btn-primary" value="SEND MESSAGE">
								<img class="ajax-loader" id="loader" src="images/ajax-loader.gif" alt="Sending ..." style="display: none;">
							</div>
						</div>
						<div class="response success">Your message was sent; we'll be in touch shortly!</div>
						<div class="response error">Unfortunately, we could not sent your message right now. Please try again.</div>
					</form>

				</div><!-- /.col -->

			</div><!-- /.row -->

		</div><!-- /.container -->

		<!-- FOOTER -->
		<?php include 'inc/footer.php'; ?>

	</div><!-- /.boxed-container -->

	<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap/carousel.js"></script>
	<script src="js/bootstrap/transition.js"></script>
	<script src="js/bootstrap/button.js"></script>
	<script src="js/bootstrap/collapse.js"></script>
	<script src="js/bootstrap/validator.js"></script>
	<script src="js/underscore.js"></script>
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/SimpleMap.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>