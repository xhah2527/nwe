-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2019 at 12:56 PM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `colossal_baby`
--

-- --------------------------------------------------------

--
-- Table structure for table `fdi`
--

CREATE TABLE `fdi` (
  `id` int(5) NOT NULL,
  `trackno` varchar(10) NOT NULL,
  `desc` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `add` text NOT NULL,
  `shipped` date NOT NULL,
  `eta` date NOT NULL,
  `origin` varchar(50) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `img_path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdi`
--

INSERT INTO `fdi` (`id`, `trackno`, `desc`, `sender`, `receiver`, `add`, `shipped`, `eta`, `origin`, `destination`, `status`, `weight`, `img_path`) VALUES
(1, '2243235', 'TextV', 'Mary Kate', 'AshleyKate', '13 Moselty', '2015-01-01', '2015-01-01', 'Lagos', 'Nigeria', 'Shipping', '473kg', 'NA'),
(2, '1310282000', 'USA', 'BRIAN CLEMENT', 'PETER PIPEK', 'HEINRICH LORENZ WASSMUTHGASSE5 A2345 BRUNN GEBIRGE AUSTRIA', '2018-11-26', '2018-11-27', '', 'AUSTRIA', 'Shipping', '75kg', 'Your package arrived in Heathrow Airport London by 7: am Tuesday, 27 November 2018 (GMT)Time in London, UK, holding up by customs for Tax clearance fee which will be pay by the receivers'),
(3, '1310282000', 'USA', 'BRIAN CLEMENT', 'PETER PIPEK', 'HEINRICH LORENZ WASSMUTHGASSE5 A2345 BRUNN GEBIRGE AUSTRIA', '2018-11-26', '2018-11-27', '', 'AUSTRIA', 'Shipping', '75kg', 'Your package arrived in Heathrow Airport London by 7: am Tuesday, 27 November 2018 (GMT)Time in London, UK, holding up by customs for Tax clearance fee which will be pay by the receivers'),
(4, '6194716546', 'Uk', 'Douglas Cox', 'William L Coxwell Jr', '66 George  Town Dr.  Apt 1 Delaware Ohio  43015 USA', '2018-11-26', '2018-11-27', '', 'USA', 'Shipping', '8.5kg', 'Your package have arrived in Northwest Alabama Regional Airport  by 2:57 am\r\nTuesday, 27 November 2018 (GMT-6)\r\nTime in Alabama, USA on process with  customs clearance'),
(5, '4349073164', 'USA', 'Facebook company', 'Benjamin Y. Lynch', '8128 Elvis Court Fort Worth Texas 7613', '2018-11-30', '2018-12-01', 'UK', 'USA', '', '9.8kg', 'Package arrived in Northwest Alabama Regional Airport by 5:30 pm\r\nFriday ,30 November 2018 (GMT-6)\r\nTime in Alabama, USA, holding under  customs custody for Tax Clearance fee which will be pay by the Receiver'),
(6, '8135157685', 'USA', 'Facebook company', 'MICHAEL LASTINGER', '201 MLK JR DR STATESBORO GEORGIA 30458 USA', '2018-12-09', '2018-12-10', 'UK', 'USA', 'Package', '9.8kg', 'Package have been transferred to IMF International Monitoring Fund Office in Istanbul Turkey because of unclaimed by the receiver.'),
(7, '5312963523', 'USA', 'Facebook company', 'Kenneth W Stelly, Sr.', '1227 Abadie St Anbebille, LA 70510 USA', '2018-11-30', '2018-12-01', 'UK', 'USA', '', '9.8kg', 'Package arrived in Northwest Alabama Regional Airport by 5:30 pm\r\nFriday ,30 November 2018 (GMT-6)\r\nTime in Alabama, USA, holding under  customs custody for Tax Clearance fee which will be pay by the Receiver'),
(8, '8172625724', 'Uk', 'ANTHONY MOORE', 'YVES JONCAS', '599 LEON MARTEL TERREBONNE QUEBEC CANADA J6W2J9', '2018-12-05', '2018-12-06', 'UK', 'CANADA', 'SHIPPING', '8.5kg', 'Le colis est arrivé à l\'aéroport international de Baltimore - Washington, Maryland le mercredi 5 décembre 2018 (GMT-5) à 15 h 29 dans le Maryland (États-Unis) dans l\'attente du paiement d\'un certificat anti-blanchiment exigé du fonds monétaire international,Union USA.'),
(9, '7253257431', 'TX', 'BRIAN CLEMENT', 'DONNA PITMON', 'Fiesta Inn &Suites  4934 N. W. Loop 410 San Antonio Texas 78229', '2010-12-08', '2018-12-08', 'NYC', 'TEXAS', 'SHIPPING', '9.8kg', 'Package arrived at Dallas/Fort Worth International Airport,12:15 am Saturday, 8 December 2018 (GMT-6) Time in Dallas, TX, USA waiting for the payment for Anti Terroist   Money laundering certificate for international monetary fund.');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) NOT NULL,
  `trackno` varchar(15) NOT NULL,
  `status` text NOT NULL,
  `location` varchar(100) NOT NULL,
  `timestamp` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fdi`
--
ALTER TABLE `fdi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fdi`
--
ALTER TABLE `fdi`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
