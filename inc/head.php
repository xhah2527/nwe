    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>New World Express : Secured and Private Logistics</title>

    <!-- Styling -->
    <link href="css/style.css" rel="stylesheet">
  <link href="css/magnific-popup.css" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Roboto%3A400%2C700%7CSource+Sans+Pro%3A700%2C900&amp;subset=latin" rel="stylesheet">

  <script src="js/modernizr.custom.24530.js" type="text/javascript"></script>

  <link rel="shortcut icon" href="images/fav.png">