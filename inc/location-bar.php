<?php

 $addresses = [
   "USA_CONTACT_1" => [
     "title" => "USA",
     "address" => "210 N Sycamore St, Coffeyville, KS 67337 @Watco Regions Warehouse"
   ],
   "USA_CONTACT_2" => [
     'title' => "USA",
     "address" => "333 W. Pioneer Drive, Joplin, MO 64801 @ Pioneer Warehouse & Logistics"
   ],
   "USA_CONTACT_3" => [
     'title' => "USA",
     "address" => "121 Harwood Dr, Newport News, VA 23603 @ Interstate Warehousing"
   ],
   "UK_CONTACT" => [
     "title" => "London",
    "address" => "K Warehouse, 2 Western Gateway, Royal Docks, London E16 1DR, UK @Warehouse K"
   ],
   "UK_CONTACT_2" => [
      "title" => "UK",
      "address" => "Foxdenton Ln, Middleton, Manchester M24 1QR, UK @ COMMERCIAL STORAGE"
   ],
   "ASIA_CONTACT" => [
     "title" => "Taiwan",
     "address" => "557, Taiwan, Nantou County, Zhushan Township @ Warehouse"
   ]
 ];
?>

<div class="col-sm-3 hentry">
  <div class="widget_black-studio-tinymce">
    <div class="featured-widget">
      <h3 class="widget-title">
        <span class="widget-title__inline">SUPPORT</span>
      </h3>
      <p>
        <strong>New World Express.</strong><br>
        <br>
      </p>
      <p>
        (515) 717-0468<br>
        (515) 717-0468<br>
        <a href="#">customer.care@newworldexpress.org.in</a>
      </p>
    </div>
  </div>				
</div>
<?php
 foreach ($addresses as $key => $location):
?>

<div class="col-sm-3 hentry">

  <div class="widget_black-studio-tinymce">
    <div class="featured-widget">
      <h3 class="widget-title">
        <span class="widget-title__inline"><?php echo $location['title']; ?></span>
      </h3>
      <p>
        <?php
            echo $location['address']; 
        ?>
        <br>
      </p>
      <p>
        <a href="#">Secure storage locations (Members Only)</a>
      </p>
    </div>
  </div>
</div><!-- /.col -->

<?php
  endforeach;
?>