<footer class="footer">
      <div class="footer-bottom">

        <div class="container">
          <div class="footer-bottom__left">
            New World Express Worldwide website designed by.
          </div>

          <div class="footer-bottom__right">
            Copyright &copy; 2016&ndash;2019 All rights reserved.
          </div>
        </div><!-- /.container -->

      </div><!-- /.footer-bottom -->

    </footer>