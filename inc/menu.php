        <header class="header">

          <div class="header__logo">
            <a href="/">
              <img class="img-responsive" srcset="images/logo.png" alt="CargoPress" src="images/logo.png" style="width: 50%;">
            </a>
            <button data-target="#cargopress-navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="navbar-toggle__text">MENU</span>
              <span class="navbar-toggle__icon-bar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </span>
            </button>
          </div><!-- /.header__logo -->

          <div class="header__navigation">
            <nav class="collapse navbar-collapse" id="cargopress-navbar-collapse">
              <ul class="main-navigation js-main-nav js-dropdown">
                <li class="">
                  <a href="index.php">Home</a>
                </li>
                <li class="">
                  <a href="services.php">Services</a>
                </li>
                <li><a href="about.php">About</a></li>
                <li><a href="insurance.php">Insurance</a></li>
                <li><a href="tracking.php">Track & Trace</a></li>
                <li><a href="contact.php">Contact</a></li>
              </ul>
            </nav>
          </div><!-- /.header__navigation -->

          <div class="header__widgets">

            <div class="widget-icon-box">

              <div class="icon-box">
                <i class="fa fa-headphones"></i>
                <h4 class="icon-box__title">Call Us Anytime</h4>
                <span class="icon-box__subtitle">(515) 717-0468</span>
              </div>

            </div>

            <div class="widget-icon-box">

              <div class="icon-box">
                <i class="fa fa-clock-o"></i>
                <h4 class="icon-box__title">Opening Time</h4>
                <span class="icon-box__subtitle">08:00 - 18:00</span>
              </div>

            </div>

            <div class="widget-icon-box">

              <div class="icon-box">
                <i class="fa fa-envelope-o"></i>
                <h4 class="icon-box__title">Email Us</h4>
                <span class="icon-box__subtitle">customer.care@newworldexpress.org.in</span>
              </div>

            </div>

            <a href="tracking.php" class="btn btn-info" id="button_requestQuote">QUICK TRACK</a>

          </div><!-- /.header__widgets -->

          <div class="header__navigation-widgets">
            <a target="_blank" href="#" class="social-icons__link"><i class="fa fa-facebook"></i></a>
            <a target="_blank" href="#" class="social-icons__link"><i class="fa fa-twitter"></i></a>
            <a target="_blank" href="#" class="social-icons__link"><i class="fa fa-wordpress"></i></a>
            <a target="_blank" href="#" class="social-icons__link"><i class="fa fa-youtube"></i></a>
          </div>

        </header>
